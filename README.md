# poc-passaporto-vaccinale

Proof of concept di un passaporto vaccinale amichevole con i diritti digitali e GDPR
Il progettino è pensato per funzionare solo su GNU/Linux e richiede la presenza dei comandi `openssl` e `qrencode` 
Non è niente di serio e completo. È solo una piccola demo di come potrebbe funzionare

Per generare il certificato vaccinale eseguire
`
./gen-certificato-vaccinale.sh 
`

Per verificare il certificato vaccinale eseguire
`
./verifica-certificato-vaccinale.sh 
`

## avanzate
Per rigenerare le chiavi dello stato
`
./gen-chiavi-stato.sh
`

Per rigenerare le chiavi dell'ASL 
`
./gen-chiavi-asl.sh
`
