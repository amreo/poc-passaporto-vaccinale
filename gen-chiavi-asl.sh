#!/bin/sh
openssl genrsa -out asl.pem 2048
openssl rsa -in asl.pem -pubout > asl.pub.pem
openssl req -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.org" -new -key asl.pem -out asl.csr
openssl x509 -req -days 365 -in asl.csr -signkey stato.pem -out asl.crt
rm asl.csr
