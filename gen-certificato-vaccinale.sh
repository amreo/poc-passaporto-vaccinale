#!/bin/sh
echo -n "Inserire comune carta identità: "
read COMUNE
echo -n "Inserire numero carta identità: "
read CID
ID_CERTIFICATO=$(cat /proc/sys/kernel/random/uuid)

echo "IT,Bergamo,$COMUNE,$CID,$ID_CERTIFICATO" > temp1.txt
openssl dgst -sign asl.pem -keyform PEM -sha256 -binary temp1.txt | base64 > temp2.txt


cat temp1.txt > certificato-vaccinale.txt
cat temp2.txt >> certificato-vaccinale.txt

# cat final.txt | gzip > final.txt.gz
# split -dC 500 final.txt final.txt.

qrencode -l H -8 -o certificato-vaccinale.png -r certificato-vaccinale.txt

xdg-open certificato-vaccinale.png