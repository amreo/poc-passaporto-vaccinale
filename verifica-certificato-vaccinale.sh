#!/bin/sh
echo "Lettura in corso dal qrcode... (in realtà direttamente dal file...)"
cat certificato-vaccinale.txt
cat certificato-vaccinale.txt | head -n 1 > certificato-header.txt
cat certificato-vaccinale.txt | tail -n +2 | base64 -d > certificato.txt.sign

STATO=$(cat certificato-header.txt | cut -d',' -f1)
ASL=$(cat certificato-header.txt | cut -d',' -f2)
COMUNE=$(cat certificato-header.txt | cut -d',' -f3)
CID=$(cat certificato-header.txt | cut -d',' -f4)
ID=$(cat certificato-header.txt | cut -d',' -f5)
echo "Questo certificato dice che è stato emesso da $ASL per il signore registrato al comune di $COMUNE di $STATO e con numero carta identità $CID. ID certificato = $ID. Verifica in corso"

if [ "$STATO" == "IT" ] && [ "$ASL" == "Bergamo" ]; then
    echo "ASL esistente"
    openssl dgst -verify asl.pub.pem -keyform PEM -sha256 -signature certificato.txt.sign -binary certificato-header.txt
    if [ $? == 0 ]; then
        echo "Certificato valido. Procedere con la l'identificazione manuale e corrispondenza tra il certificato vaccinale e la carta identità"
    else 
        echo "Certificato invalido..."
    fi
else
    echo "Certificato falso. L'asl non esiste..."
fi